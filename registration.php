<?php
  require 'config/config.php';
?>
<!DOCTYPE html>
<html>
  <head>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800|Playfair+Display:,300, 400, 700" rel="stylesheet">

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">

<link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">


<!-- Theme Style -->
<link rel="stylesheet" href="css/style.css">
  </head>
  <body>
  <div class="row">
          <div class="col-md-12">
          <form action="registration.php" method="post">
            <div class="row mb-4">
              <div class="col-md-4 mb-md-0 mb-4">
              <input type="text" name="voornaam" class="form-control" id="voornaam" placeholder="Voornaam" required/>
              </div>
              <div class="col-md-4 mb-md-0 mb-4">
                <input type="text" name="tussenvoegsel" class="form-control" id="tussenvoegsel" placeholder="Tussenvoegsel"/>
              </div>
              <div class="col-md-4 mb-md-0 mb-4">
                <input type="text" name="achternaam" id="achternaam" class="form-control" placeholder="Achternaam" required/>
              </div>
            </div>
            <div class="row mb-4">
              <div class="col-md-4 mb-md-0 mb-4">
                <input type="text" name="username" class="form-control" id="username" placeholder="Username" required/>
              </div>
              <div class="col-md-4 mb-md-0 mb-4">
                <input type="date" name="geboortedatum" class="form-control" id="geboortedatum" placeholder="Geboortedatum" required/>
              </div> 
              <div class="col-md-4 mb-md-0 mb-4">
              <input type="password" name="password" class="form-control" id="password" placeholder="Password" required/>
              </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-4 mb-md-0 mb-4">
                  <input type="email" name="email" id="email" class="form-control" placeholder="E-mail" required/>
                </div> 
                <!-- Genders-->
              <div class="col-md-4 mb-md-0 mb-4">
                <select id="gender" name="gender" class="form-control">
                  <option value="man" name="man" id="man">Man</option>
                  <option value="vrouw">Vrouw</option>
                  <option value="anders">Anders</option>
                </select>   
              </div>                                      
                <div class="col-md-4">
                    <input name="submit" type="submit" class="btn btn-primary btn-block" value="Registreer">
                </div>
            </div>
          </form>
          </div>
        </div>
  </body>
</html>

<?php

        // Check connection
        if($conn === false){
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }
    
        if(isset($_POST["submit"])){

        $user_voornaam = mysqli_real_escape_string($conn, $_REQUEST["voornaam"]);
        $user_tussenvoegsel = mysqli_real_escape_string($conn, $_REQUEST["tussenvoegsel"]);
        $user_achternaam = mysqli_real_escape_string($conn, $_REQUEST["achternaam"]);
        $user_email = mysqli_real_escape_string($conn, $_REQUEST["email"]);
        $user_geslacht = mysqli_real_escape_string($conn, $_REQUEST["gender"]);
        $user_wachtwoord = mysqli_real_escape_string($conn, $_REQUEST["password"]);
        $user_geboortedatum = mysqli_real_escape_string($conn, $_REQUEST["geboortedatum"]);
        $user_username = mysqli_real_escape_string($conn, $_REQUEST["username"]);
        
        
    
        $sql = "INSERT INTO users (voornaam, tussenvoegsel, achternaam, email, geslacht, `password`, geboortedatum, registratie, username) VALUES ('$user_voornaam', '$user_tussenvoegsel', '$user_achternaam', '$user_email', '$user_geslacht', '$user_wachtwoord', '$user_geboortedatum', NOW(), '$user_username')"; 
        
        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        $conn->close();
    }

?>