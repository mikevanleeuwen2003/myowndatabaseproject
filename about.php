<!doctype html>
<html lang="en">
  <head>
    <link rel="icon" href="img/logo.png" type="image/gif">
    <title>PinkPanda EventManager</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800|Playfair+Display:,300, 400, 700" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">

    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">


    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    
    <header role="banner">
      <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container">
          <a class="navbar-brand" href="index.php">PinkPanda</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarsExample05">
            <ul class="navbar-nav ml-auto pl-lg-5 pl-0">
            <li class="nav-item">
                <a class="nav-link " href="index.php">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">Wie zijn wij</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="events.php">Alle Events</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="form2.php">Maak Event</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="delete_events.php">Delete Event</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="registration.php">Registreren</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="login.php">Log in</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <!-- END header -->
    <section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('img/slider-1.jpg');">
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-md-8 text-center col-sm-12 element-animate">
              <h1>Pinkpanda</h1>
              <p class="mb-5">De beste website om leuke dingen te doen, op werkdagen en in het weekend!</p>
            </div>
          </div>
        </div>
      </div>
      <div class="slider-item" style="background-image: url('img/slider-2.jpg');">
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-md-8 text-center col-sm-12 element-animate">
              <h1>Pinkpanda</h1>
              <p class="mb-5">De beste website om leuke dingen te doen, op werkdagen en in het weekend!</p>
            </div>
          </div>
        </div>
      </div>
    </section>   
    <!-- END slider -->

    <section class="section pt-md-5 pb-md-5 element-animate">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-12">
            <span class="section-heading">Het begin</span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <p></p>
            <p></p>
            <h2 class="mb-4"></h2>
            <p></p>
          </div>
          <div class="col-md-6">
            <p><img src="img/founder.jpg" alt="Image placeholder" class="img-fluid"></p>
            
          </div>
        </div>
        <div class="row">
        </div>
      </div>
    </section>
    <!-- END section -->

    <section class="section">
      <div class="container">
        <div class="row mb-5 justify-content-center element-animate">
          <div class="col-md-12">
            <span class="section-heading">Creators</span>
            <h2>Meet Our Creators</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate">
            <div class="media pastor d-block text-center">
              <div class="media-body">
                <h3>Marco Molenaar</h3>
                <p class="position">SCRUM Master</p>
                <p>Marco is de </p>
              </div>
            </div>
          </div>
          <div class="col-md-4 element-animate">
            <div class="media pastor d-block text-center">
              <div class="media-body">
                <h3>Mike van Leeuwen</h3>
                <p class="position">Church Pastor</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat vel nam natus dolore fugit, dolores explicabo nemo in, laboriosam incidunt, repudiandae! Aut, accusantium nihil facilis ducimus quas autem fugit odio?</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 element-animate">
            <div class="media pastor d-block text-center">
              <div class="media-body">
                <h3>Joost Verleun</h3>
                <p class="position">Church Pastor</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat vel nam natus dolore fugit, dolores explicabo nemo in, laboriosam incidunt, repudiandae! Aut, accusantium nihil facilis ducimus quas autem fugit odio?</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 element-animate">
            <div class="media pastor d-block text-center">
              <div class="media-body">
                <h3></h3>
                <p class="position"></p>
                <p></p>
              </div>
            </div>
          </div>
          <div class="col-md-4 element-animate">
            <div class="media pastor d-block text-center">
              <div class="media-body">
                <h3>Yuri Barros</h3>
                <p class="position">Church Pastor</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat vel nam natus dolore fugit, dolores explicabo nemo in, laboriosam incidunt, repudiandae! Aut, accusantium nihil facilis ducimus quas autem fugit odio?</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- <section class="section pt-5 pb-2">
      <div class="container">
        <div class="row mb-5 justify-content-center element-animate">
          <div class="col-md-12">
            <span class="section-heading">Recent Sermons</span>
            <h2>Listen Our Sermons</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-6 col-lg-4 mb-5">
            
            <div class="sermon element-animate">
              <div class="sermon-text">
            <h2><a href="#">Arise, Shine</a></h2>
            <p class="sermon-meta">by <a href="#">Luis Matthew</a> on March 28, 2018</p>
            </div>
            <div class="player">
                <audio id="player2" preload="none" controls style="max-width: 100%">
                    <source src="http://www.largesound.com/ashborytour/sound/AshboryBYU.mp3" type="audio/mp3">
                </audio>
            </div>
            </div>

          </div>
          <div class="col-12 col-sm-6 col-lg-4 mb-5">
            
            <div class="sermon element-animate">
              <div class="sermon-text">
            <h2><a href="#">Filled in Him</a></h2>
            <p class="sermon-meta">by <a href="#">Luis Matthew</a> on March 28, 2018</p>
            </div>
            <div class="player">
                <audio id="player3" preload="none" controls style="max-width: 100%">
                    <source src="http://www.largesound.com/ashborytour/sound/AshboryBYU.mp3" type="audio/mp3">
                </audio>
            </div>
            </div>

          </div>
          <div class="col-12 col-sm-6 col-lg-4 mb-5">
            
            <div class="sermon element-animate">
              <div class="sermon-text">
            <h2><a href="#">Jehovah the Creator</a></h2>
            <p class="sermon-meta">by <a href="#">Luis Matthew</a> on March 28, 2018</p>
            </div>
            <div class="player">
                <audio id="player4" preload="none" controls style="max-width: 100%">
                    <source src="http://www.largesound.com/ashborytour/sound/AshboryBYU.mp3" type="audio/mp3">
                </audio>
            </div>
            </div>

          </div>
        </div>
      </div>
    </section> -->

    <!-- END section -->
    
    

    <footer class="site-footer" role="contentinfo">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-4 mb-5">
            <h3></h3>
            <p class="mb-5"></p>
            <ul class="list-unstyled footer-link d-flex footer-social">
            </ul>
          </div>
          <div class="col-md-5 mb-5">
            <h3>Contact Info</h3>
            <ul class="list-unstyled footer-link">
              <li class="d-block">
                <span class="d-block">Address:</span>
                <span class="text-white">Koningin Wilhelminalaan 7, Utrecht, Nederland</span></li>
              <li class="d-block"><span class="d-block">Telephone:</span><span class="text-white"> - </span></li>
              <li class="d-block"><span class="d-block">Email:</span><span class="text-white">pinkpandabv@gmail.com</span></li>
            </ul>
          </div>
          
        </div>
        <div class="row">
          <div class="col-12 text-md-center text-left">
            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> <br> Demo Images Unsplash</p>
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->

    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelement-and-player.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/main.js"></script>

    <script>
      document.addEventListener('DOMContentLoaded', function() {
                var mediaElements = document.querySelectorAll('video, audio'), total = mediaElements.length;

                for (var i = 0; i < total; i++) {
                    new MediaElementPlayer(mediaElements[i], {
                        pluginPath: 'https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/',
                        shimScriptAccess: 'always',
                        success: function () {
                            var target = document.body.querySelectorAll('.player'), targetTotal = target.length;
                            for (var j = 0; j < targetTotal; j++) {
                                target[j].style.visibility = 'visible';
                            }
                  }
                });
                }
            });
    </script>
  </body>
</html>