<?php
 require "config/config.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="img/logo.png" type="image/gif">
    <link rel="stylesheet" href="css/form_add.css">
    <title>From_2</title>
</head>
<body background-color="#435165">
    <div id="container">
        <h1>Stuur een bericht</h1>
        <form action="form3.php" method="post" enctype="multipart/form-data">
            <!--event name-->
            <input class="input_field" type="text" name="event_name" placeholder="Event name*" required>
            <!--image-->
            <input class="input_field" type="file" id="fileToUpload" name="fileToUpload" placeholder="Image*">
            <!--date-->
            <input class="input_field" type="date" name="event_time" placeholder="Date*" required>
            <!--event-->
            <input class="input_field" type="number" name="event_price" placeholder="Price*" required>
            <!--event beschrijving-->
            <textarea rows="7" cols="24" name="event_info" placeholder="Event Beschrijving*" required></textarea>
            <!--Submit-->
            <input type="submit" name="submit">
        </form>
    </div>
</body>
</html>
<?php

    // Check connection
    if($conn === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }

    if(isset($_POST["submit"])){
    $img = "";
    $target_dir = $_SERVER['DOCUMENT_ROOT']."/pinkpanda/uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }   
        
        // Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        $img =  basename( $_FILES["fileToUpload"]["name"]);
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

    $event_name = mysqli_real_escape_string($conn, $_REQUEST["event_name"]);
    $event_info = mysqli_real_escape_string($conn, $_REQUEST["event_info"]);
    $event_time = mysqli_real_escape_string($conn, $_REQUEST["event_time"]);
    $event_img = mysqli_real_escape_string($conn, $img);
    $event_price = mysqli_real_escape_string($conn, $_REQUEST["event_price"]);
    

    $sql = "INSERT INTO events (event_name, event_info, event_time, event_price, event_img)
    VALUES ('$event_name', '$event_info', '$event_time', '$event_price', '$event_img');";
    
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    $conn->close();
}
?>