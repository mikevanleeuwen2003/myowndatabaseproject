<?php
 require "config/config.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="img/logo.png" type="image/gif">
    <title>From_2</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800Playfair+Display:,300, 400, 700" rel="stylesheet">

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">

<link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">


<!-- Theme Style -->
<link rel="stylesheet" href="css/style.css">
</head>
        <div class="row">
          <div class="col-md-12">
          <form action="form2.php" method="post" enctype="multipart/form-data">
            <div class="row mb-4">
              <div class="col-md-4 mb-md-0 mb-4">
                <input  type="text" name="event_name" class="form-control" placeholder="Event name*" required>
              </div>
              <div class="col-md-4 mb-md-0 mb-4">
                <input  type="file" id="fileToUpload" class="form-control" name="fileToUpload" placeholder="Image*">
              </div>
              <div class="col-md-4 mb-md-0 mb-4">  
              </div>
              <br><br><br>
              <div class="col-md-4 mb-md-0 mb-4">
                <input type="date" name="event_time" class="form-control" placeholder="Date*" required>    
              </div>
              <div class="col-md-4 mb-md-0 mb-4">
                <input  type="number" name="event_price"class="form-control"  placeholder="Price*" required>      
              </div>
            </div>
            <div class="row mb-4">
              <div class="col-md-12">
                <textarea rows="10" cols="120" name="event_info" placeholder="Event Beschrijving*" required></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <Button name="submit" type="submit" class='btn btn-primary btn-block'>Maak event</button>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </section>

</body>
</html>
<?php

    // Check connection
    if($conn === false){
        die("ERROR: Could not connect. " . mysqli_connect_error());
    }

    if(isset($_POST["submit"])){
    $img = "";
    $target_dir = $_SERVER['DOCUMENT_ROOT']."/pinkpanda/uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }   
        
        // Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        $img =  basename( $_FILES["fileToUpload"]["name"]);
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

    $event_name = mysqli_real_escape_string($conn, $_REQUEST["event_name"]);
    $event_info = mysqli_real_escape_string($conn, $_REQUEST["event_info"]);
    $event_time = mysqli_real_escape_string($conn, $_REQUEST["event_time"]);
    $event_img = mysqli_real_escape_string($conn, $img);
    $event_price = mysqli_real_escape_string($conn, $_REQUEST["event_price"]);
    

    $sql = "INSERT INTO events (event_name, event_info, event_time, event_price, event_img)
    VALUES ('$event_name', '$event_info', '$event_time', '$event_price', '$event_img');";
    
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    $conn->close();
}
?>