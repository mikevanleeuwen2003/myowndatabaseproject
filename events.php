<?php
    require "config/config.php";
?>

<!doctype html>
<html lang="en">
  <head>
    <link rel="icon" href="img/logo.png" type="image/gif">
    <title>PinkPanda EventManager</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800|Playfair+Display:,300, 400, 700" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">

    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">


    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    
    <header role="banner">
      <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container">
          <a class="navbar-brand" href="index.php">PinkPanda</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarsExample05">
            <ul class="navbar-nav ml-auto pl-lg-5 pl-0">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">Wie zijn wij</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="events.php">Alle Events</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="form2.php">Maak Event</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="delete_events.php">Delete Event</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="registration.php">Registreren</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="login.php">Log in</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <!-- END header -->
        <section class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url('img/slider-1.jpg');">
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-md-8 text-center col-sm-12 element-animate">
              <h1>Pinkpanda</h1>
              <p class="mb-5">De beste website om leuke dingen te doen, op werkdagen en in het weekend!</p>
            </div>
          </div>
        </div>
      </div>
      <div class="slider-item" style="background-image: url('img/slider-2.jpg');">
        <div class="container">
          <div class="row slider-text align-items-center justify-content-center">
            <div class="col-md-8 text-center col-sm-12 element-animate">
              <h1>Pinkpanda</h1>
              <p class="mb-5">De beste website om leuke dingen te doen, op werkdagen en in het weekend!</p>
            </div>
          </div>
        </div>
      </div>
    </section>   
    <!-- END slider -->

    <section class="section">
      <div class="container">
        <div class="row mb-5 justify-content-center element-animate">
          <div class="col-md-12">
            <span class="section-heading">Ministries</span>
            <h2>Events &amp; Ministries</h2>
          </div>
        </div>

        <div class="row">

        <?php
            $sql = "SELECT * FROM events";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                  echo "
                  <div class='col-md-6 col-lg-4 mb-4 element-animate'>
                  <div class='media custom-media'>
                    <img class='mr-3' width='30' src='uploads/" . $row["event_img"] . "' alt='Generic placeholder image'>
                    <div class='media-body'>
                      <h3 class= 'mt-0'><a href='#'>" . $row["event_name"] . "</a></h3>
                      <p class='post-meta'>" . $row["event_time"] . ", <a href='#'>$" . $row["event_price"] . "</a></p>
                      <p>" . $row["event_info"] . "</p>
                    </div>
                  </div>
                </div>
                  ";
                }
            } else {
                echo "No events found!";
            }
        ?>

        </div>
      </div>
    </section>

    <section class="section">
      <div class="container">
        <div class="row mb-5 justify-content-center element-animate">
          <div class="col-md-12">
            <span class="section-heading">Officers</span>
            <h2>Officers For Children Ministries</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate">
            <div class="media pastor d-block text-center">
              <img src="img/pastor_1.jpg" alt="Image placeholder" class="img-fluid mb-4">
              <div class="media-body">
                <h3>Luis Matthew</h3>
                <p class="position">Head of Children Ministry</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat vel nam natus dolore fugit, dolores explicabo nemo in, laboriosam incidunt, repudiandae! Aut, accusantium nihil facilis ducimus quas autem fugit odio?</p>
                <p><a href="#" class="btn btn-primary">Read Bio</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-4 element-animate">
            <div class="media pastor d-block text-center">
              <img src="img/pastor_2.jpg" alt="Image placeholder" class="img-fluid mb-4">
              <div class="media-body">
                <h3>Chris Mattwood</h3>
                <p class="position">Children Trainer</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat vel nam natus dolore fugit, dolores explicabo nemo in, laboriosam incidunt, repudiandae! Aut, accusantium nihil facilis ducimus quas autem fugit odio?</p>
                <p><a href="#" class="btn btn-primary">Read Bio</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-4 element-animate">
            <div class="media pastor d-block text-center">
              <img src="img/pastor_3.jpg" alt="Image placeholder" class="img-fluid mb-4">
              <div class="media-body">
                <h3>Jack Winston</h3>
                <p class="position">Child Dedication Officer</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat vel nam natus dolore fugit, dolores explicabo nemo in, laboriosam incidunt, repudiandae! Aut, accusantium nihil facilis ducimus quas autem fugit odio?</p>
                <p><a href="#" class="btn btn-primary">Read Bio</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
          </div>
        </div>
      </div>
    </section>

    <!-- END section -->
    </section>
    

    <footer class="site-footer" role="contentinfo">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-4 mb-5">
            <h3></h3>
            <p class="mb-5"></p>
            <ul class="list-unstyled footer-link d-flex footer-social">

            </ul>
          </div>
          <div class="col-md-5 mb-5">
            <h3>Contact Info</h3>
            <ul class="list-unstyled footer-link">
              <li class="d-block">
                <span class="d-block">Address:</span>
                <span class="text-white">Koningin Wilhelminalaan 7, Utrecht, Nederland</span></li>
              <li class="d-block"><span class="d-block">Telephone:</span><span class="text-white"> - </span></li>
              <li class="d-block"><span class="d-block">Email:</span><span class="text-white">pinkpandabv@gmail.com</span></li>
            </ul>
          </div>
          
        </div>
        <div class="row">
          <div class="col-12 text-md-center text-left">
            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> <br> Demo Images Unsplash</p>
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->

    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelement-and-player.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/main.js"></script>

    <script>
      document.addEventListener('DOMContentLoaded', function() {
                var mediaElements = document.querySelectorAll('video, audio'), total = mediaElements.length;

                for (var i = 0; i < total; i++) {
                    new MediaElementPlayer(mediaElements[i], {
                        pluginPath: 'https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/',
                        shimScriptAccess: 'always',
                        success: function () {
                            var target = document.body.querySelectorAll('.player'), targetTotal = target.length;
                            for (var j = 0; j < targetTotal; j++) {
                                target[j].style.visibility = 'visible';
                            }
                  }
                });
                }
            });
    </script>
  </body>
</html>